DOCKER_USER = group20
COMPOSE_PROJECT_NAME = ccsep-group20-assignment
DOCKER_IMAGE_PREFIX = ${DOCKER_USER}/assignment
export DOCKER_IMAGE_NAME_BANK_DEV = $(DOCKER_IMAGE_PREFIX)_bank_dev
export DOCKER_IMAGE_NAME_FRONTEND_DEV = $(DOCKER_IMAGE_PREFIX)_frontend_dev
export DOCKER_IMAGE_NAME_BANK_PROD = $(DOCKER_IMAGE_PREFIX)_bank_prod
export DOCKER_IMAGE_NAME_FRONTEND_PROD = $(DOCKER_IMAGE_PREFIX)_frontend_prod
export DOCKER_IMAGE_NAME_BANK_TEST = $(DOCKER_IMAGE_PREFIX)_bank_test
export DOCKER_IMAGE_NAME_FRONTEND_TEST = $(DOCKER_IMAGE_PREFIX)_frontend_test

export BANK_TAG = 1.0.0
export FRONTEND_TAG = 1.0.0

# Enable BuildKit, and tell docker-compose to use Docker via the CLI
export DOCKER_BUILDKIT = 1
export COMPOSE_DOCKER_CLI_BUILD = 1

COMPOSE = docker-compose -p $(COMPOSE_PROJECT_NAME)
COMPOSE_PROD = $(COMPOSE) -f ./docker-compose.yml -f ./docker-compose.prod.yml
COMPOSE_TEST = $(COMPOSE) -f ./docker-compose.yml -f ./docker-compose.test.yml

default: help

.PHONY: stop-all stop-bank stop-front docker-images docker-images-clean docker-clean dev-start dev-start-d dev-list dev-logs dev-start-bank dev-start-bank-d dev-logs-bank dev-start-front dev-start-front-d dev-logs-front prod-start prod-start-d prod-list prod-logs prod-logs-bank prod-logs-front test-build-bank test-run-bank test-logs-bank help

# DOCKER STUFF

stop-all: ## Stop all running Docker containers
	$(COMPOSE) down

stop-bank: ## Stop bank container
	$(COMPOSE) down spring-bank

stop-front: ## Stop front container
	$(COMPOSE) down react-frontend

docker-images: ## List Docker images
	@docker images --filter=reference='$(DOCKER_USER)/*'

docker-images-clean: ## Clean dangling images (tagged as <none>)
	@docker rmi $(shell docker images -q --filter="dangling=true")

docker-clean: ## Remove all images, volumes, networks and caches related to the docker-compose files
	@docker rmi $(docker images --filter=reference='group20/*')
	$(COMPOSE) down -v --rmi all
	$(COMPOSE_PROD) down -v --rmi all
	$(COMPOSE_TEST) down -v --rmi all
	@docker builder prune -f

# DEV MODE COMMANDS

dev-start: ## Build / start all dev containers
	$(COMPOSE) up --build

dev-start-d: ## Build / start all dev containers (in daemon mode)
	$(COMPOSE) up -d --build

dev-list: ## List all dev containers
	$(COMPOSE) ps

dev-logs: ## Show all logs (dev)
	$(COMPOSE) logs -f

# DEV MODE - SPECIFIC CONTAINERS

## BANK

dev-start-bank: ## Start bank (dev)
	$(COMPOSE) up --build spring-bank

dev-start-bank-d: ## Start bank (dev, daemon mode)
	$(COMPOSE) up --build -d spring-bank

dev-logs-bank: ## Show bank logs (dev)
	$(COMPOSE) logs -f spring-bank

## FRONTEND

dev-start-front: ## Start front (dev)
	$(COMPOSE) up --build react-frontend

dev-start-front-d: ## Start front (dev, daemon mode)
	$(COMPOSE) up --build -d -react-frontend

dev-logs-front: ## Show front logs (dev)
	$(COMPOSE) logs -f react-frontend

# PROD MODE COMMANDS

prod-start: ## Build / start all prod containers
	$(COMPOSE_PROD) up --build

prod-start-d: ## Build / start all prod containers (in daemon mode)
	$(COMPOSE_PROD) up --build -d

# TODO: maybe need no-rebuild versions?

prod-list: ## List all prod containers
	$(COMPOSE_PROD) ps

prod-logs: ## Show all logs (prod)
	$(COMPOSE_PROD) logs -f

prod-logs-bank: ## Show bank logs (prod)
	$(COMPOSE_PROD) logs -f spring-bank

prod-logs-front: ## Show front logs (prod)
	$(COMPOSE_PROD) logs -f react-frontend

# TEST MODE COMMANDS

test-build-bank:
	$(COMPOSE_TEST) build spring-bank

test-run-bank:
	$(COMPOSE_TEST) up spring-bank

test-logs-bank: ## Show bank logs (test)
	$(COMPOSE_TEST) logs -f spring-bank

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-22s\033[0m %s\n", $$1, $$2}'
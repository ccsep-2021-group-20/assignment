import { Component } from 'react';
import './App.css';
import { BankAppBar, BodyPage } from "./components/index";



class App extends Component {
  constructor(){
    super();

    this.handleLogin = this.handleLogin.bind(this);

    this.state = {
      loggedInStatus: "NO",
      user: {}
    };
  }

  handleLogin(data){
    this.setState({
      loggedInStatus: "YES",
      user: data.user
    });
  }

  render(){
    return (
      <div className="App">
        <BodyPage />
      </div>
  )}
}

export default App;

import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Button, Container } from '@material-ui/core';

export default class CSRF extends React.Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleComplete = this.handleComplete.bind(this);

        this.state = {
            csrf: props.csrf,
        }
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleComplete(event){
        event.preventDefault();
        const { csrf } = this.state;
        this.props.handleComplete( csrf );
    }

    render(){
        return <div><Container component="main" maxWidth="xs"><form onSubmit={this.handleComplete}>
            <p><br /></p>
            <h2>CSRF: </h2><TextField id="csrf" name="csrf" onChange={this.handleChange} defaultValue={this.props.csrf} /><p><br /></p>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
            >
                Send
            </Button>
        </form></Container></div>
    }
}
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Button, Container } from '@material-ui/core';

export default class Transfer extends React.Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleComplete = this.handleComplete.bind(this);

        this.state = {
            api: "api",
            target: "default_username",
            ammount: "0",
            csrf: props.csrf,
        }
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleComplete(event){
        event.preventDefault();
        const { api, target, ammount, csrf } = this.state;
        this.props.handleComplete( api, target, ammount, csrf );
    }

    render(){
        return <div><Container component="main" maxWidth="xs"><form onSubmit={this.handleComplete}>
            <p><br /></p>
            <h2>API: </h2><TextField id="api" name="api" onChange={this.handleChange} defaultValue="api" /><p><br /></p>
            <h2>Transfer Target: </h2><TextField id="target" name="target" onChange={this.handleChange} /><p><br /></p>
            <h2>Transfer Ammount: </h2><TextField id="ammount" name="ammount" onChange={this.handleChange} /><p><br /></p>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
            >
                Send
            </Button>
        </form></Container></div>
    }
}
import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';


export default class SignIn extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            username: "default_username",
            password: "default_password"
        }


        this.handleChange = this.handleChange.bind(this);
        this.handleLoginClicked = this.handleLoginClicked.bind(this);
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleLoginClicked(event) {
        event.preventDefault();  
        const { username, password } = this.state;
        this.props.handleLogin(username, password);
    }

    render(){
    return <div>
            <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div>
                <Typography component="h1" variant="h5">
                <br />Sign in
                </Typography>
                <form onSubmit={this.handleLoginClicked}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="username"
                    autoComplete="email"
                    autoFocus
                    onChange={this.handleChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={this.handleChange}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                >
                    Sign In
                </Button>
                </form>
            </div>
            </Container>
        </div>
    }
}
import React from "react";
import logo from '../logo.svg';
import SignIn from "./SignIn";
import axios from 'axios';
import Transfer from "./Transfer";
import { BankAppBar } from ".";
import Balance from "./Balance";
import Cookies from 'universal-cookie';
import CSRF from "./CSRF";


export default class BodyPage extends React.Component {
    constructor(props){
        super(props);
        this.LogUserIn = this.LogUserIn.bind(this);
        this.MakeTransfer = this.MakeTransfer.bind(this);
        this.showPage = this.showPage.bind(this);
        this.GetBalance = this.GetBalance.bind(this);
        this.Logout = this.Logout.bind(this);
        this.ChangeCSRF = this.ChangeCSRF.bind(this);

        this.state = {
            displayedPage: "Login",
        }

        const cookies = new Cookies();
        const username = cookies.get("username");
        if(username !== "")
        {
            this.user = { 
                username: username, 
                password: "",
            };
        }
        else {
            this.user = {
                username: null,
                password: null,
            }
        }

        this.pages = {
            "Home": <div><header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                        <p>
                        Welcome to our Bank.<br />
                        We aren't demonstrating a phishing attack, so treat this as the real deal.  
                        </p>
                    </header></div>,
            "Login": <SignIn {... props} handleLogin={this.LogUserIn} />,
            "Transfer": null,
            "Balance": null,
            "CSRF": null,
        }

        this.showPage("Login");
    }

    LogUserIn(username, password){
        console.log("Logging in with: ", username, password);

        const URL = "http://localhost:8080/user/login?name="+username+"&password="+password;
        axios.get(URL, { withCredentials: true }
            ).then( response =>{
                response.header("Access-Control-Allow-Origin", "*");
                response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                console.log("Response:", response);
            });
        window.open(URL, "call");

        this.user = {
            username: username,
            password: password,
        }

        this.csrf = "";

        const cookies = new Cookies();
        cookies.set('username', this.user.username);

        this.showPage("Home");
    }

    MakeTransfer( api, target, ammount, csrf ){
        const username = this.user.username;
        this.csrf = csrf;
        
        if(target !== "" && ammount !== "0"){
            console.log("Transerring", ammount, "from", username, "to", target);

            const URL = "http://localhost:8080/"+api+"/transfer/?target="+target+"&amount="+ammount+"&CSRF="+csrf;
            axios.get(URL, { withCredentials: true }
                ).then( response =>{
                    response.header("Access-Control-Allow-Origin", "*");
                    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                    console.log("Response:", response);
                });
            window.open(URL, "call");
        }

        this.showPage("Home");
    }

    GetBalance(){
        const { username, password } = this.user;

        console.log("Getting Balance of: ", username, password);

        const URL = "http://localhost:8080/user/?name="+username+"&password="+password;
        axios.get(URL, { withCredentials: true }
            ).then( response =>{
                response.header("Access-Control-Allow-Origin", "*");
                response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                console.log("Response:", response);
            });
        window.open(URL, "call");

        return "0";
    }

    ChangeCSRF( csrf ){
        this.csrf = csrf;
        this.showPage("Home");
    }

    Logout(){
        console.log("Logging out");
        var cookies = new Cookies();
        cookies.remove("username");
        this.user = {
            username: null,
            password: null,
        }
        this.showPage("Login");
    }

    showPage(pageName){
        console.log("Setting page to: ", pageName);

        if(!this.user.username){
            this.setState({displayedPage: "Login"});
            return;
        }

        if(pageName === "Login" && this.user.username) {
            pageName = "Home"
        }

        if(pageName === "Login" && this.user.username && this.user.username !== "")
        {
            pageName = "Home";
        }

        if(pageName === "Transfer"){
            this.pages.Transfer = <Transfer {... this.props} handleComplete={this.MakeTransfer} csrf={this.csrf} />
        }

        if(pageName === "CSRF"){
            this.pages.CSRF = <CSRF {... this.props} handleComplete={this.ChangeCSRF} csrf={this.csrf} />
        }

        if(pageName === "Balance"){
            this.pages.Balance = <Balance {... this.props} bal={this.GetBalance()} />
        }

        console.log("Page is: ", pageName);
        this.setState({displayedPage: pageName});
    }

    render(){
        return (
            <div>
                <BankAppBar {... this.props} changePage={this.showPage} Logout={this.Logout} />
                {this.pages[this.state.displayedPage]}
            </div>
        )
    }
}
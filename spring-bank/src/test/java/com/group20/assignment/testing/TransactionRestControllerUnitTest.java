package com.group20.assignment.testing;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;

import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.RandomTokenGenerator;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;
import com.group20.assignment.domain.User;
import com.group20.assignment.controller.TransactionController;
import com.group20.assignment.controller.UserController;
import com.group20.assignment.domain.CSRFToken;
import com.group20.assignment.domain.Session;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionRestControllerUnitTest {

    @Autowired
    private UserController userController;

    @Autowired
    private TransactionController transactionController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CSRFRepository csrfRepository;


    private final String TEST_API = "api";
    private final String victim = "victim";
    private final String victim_password = "password1";
    private final String attacker = "attacker";
    private final String attacker_password = "password2";
    private final float victim_start_balance = 50000f;
    private final float attacker_start_balance = 0f;
    MockHttpServletResponse response;

    @BeforeEach
    private void resetDb() {
        
        userRepository.deleteAll();

        User user = new User(victim, victim_password, victim_start_balance);
        userRepository.saveAndFlush(user);
        User user2 = new User(attacker, attacker_password, attacker_start_balance);
        userRepository.saveAndFlush(user2);

        response = new MockHttpServletResponse();

    }


    private ResponseEntity<String> doLogin(String inName, String inPassword) throws Exception {
        ResponseEntity<String> responseEntity = userController.login(inName, inPassword, response, "");

        return responseEntity;
    }

    // Basic transaction test
    @Test
    public void doTransaction() throws Exception {
        float expectedAttackerEndBalance = victim_start_balance;
        float expectedVictimEndBalance = 0;

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();
        String session_token = response.getCookies()[1].getValue();

        assertThat(transactionController.getMethodName(attacker, victim_start_balance, csrf, session_token).getStatusCode(), is(HttpStatus.OK));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }

    // Basic invalid target user transaction test
    @Test
    public void doBadTargetTransaction() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;
        String badTarget = "bad target";

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();
        String session_token = response.getCookies()[1].getValue();

        assertThat(transactionController.getMethodName(badTarget, victim_start_balance, csrf, session_token).getStatusCode(), is(HttpStatus.NOT_FOUND));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }

    // Basic invalid amount transaction test
    @Test
    public void doBadAmountTransactionTooBig() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;
        float invalidAmount = victim_start_balance * 2;

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();
        String session_token = response.getCookies()[1].getValue();

        assertThat(transactionController.getMethodName(attacker, invalidAmount, csrf, session_token).getStatusCode(), is(HttpStatus.PAYMENT_REQUIRED));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }

    // Basic invalid negative amount transaction test
    @Test
    public void doBadAmountTransactionNegative() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;
        float invalidAmount = victim_start_balance - victim_start_balance * 2;

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();
        String session_token = response.getCookies()[1].getValue();

        assertThat(transactionController.getMethodName(attacker, invalidAmount, csrf, session_token).getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }




    // test where cookie is empty (i.e. "") 
    @Test
    public void doTransactionWithEmptyCookie() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();

        assertThat(transactionController.getMethodName(attacker, victim_start_balance, csrf, "").getStatusCode(), is(HttpStatus.UNAUTHORIZED));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }


    // test where cookie is invalid (i.e. random string)
    @Test
    public void doTransactionWithRandomInvalidCookie() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;
        String fakeCookieToken = RandomTokenGenerator.requestToken();
        

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = loginResultVictim.getBody();

        assertThat(transactionController.getMethodName(attacker, victim_start_balance, csrf, fakeCookieToken).getStatusCode(), is(HttpStatus.UNAUTHORIZED));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }
    
    // test vulnerability 2 : CSRF is random
    // checks that request contains a token, not that it was generated by the backend
    @Test
    public void doTransactionWithRandomCSRF() throws Exception {
        float expectedAttackerEndBalance = attacker_start_balance;
        float expectedVictimEndBalance = victim_start_balance;

        ResponseEntity<String> loginResultVictim = doLogin(victim, victim_password);

        String csrf = RandomTokenGenerator.requestToken();
        String session_token = response.getCookies()[1].getValue();


        assertThat(transactionController.getMethodName(attacker, victim_start_balance, csrf, session_token).getStatusCode(), is(HttpStatus.FORBIDDEN));
       
        // Check user balances are as expected
        assertThat((double) userRepository.findByName(victim).getBalance(), closeTo(expectedVictimEndBalance, 0.001));
        assertThat((double) userRepository.findByName(attacker).getBalance(), closeTo(expectedAttackerEndBalance, 0.001)); 
    }
}

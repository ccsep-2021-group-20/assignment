package com.group20.assignment.testing;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isNotNull;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;
import com.group20.assignment.controller.UserController;
import com.group20.assignment.domain.User;

import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

@SpringBootTest
public class UserRestControllerUnitTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserController userController;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CSRFRepository csrfRepository;


    private final String name = "test";
    private final String password = "password123";
    private final float balance = 500f;
    private MockHttpServletResponse response;

    private Long id;

    @BeforeEach
    private void resetDb() {
        userRepository.deleteAll();

        User user = new User(name, password, balance);
        userRepository.saveAndFlush(user);
        response = new MockHttpServletResponse();

        id = userRepository.findByNameAndPassword(name, password).getID();
    }
    
    @Test
    public void loginWithEmptyDetailsReturns404Error() throws Exception {
        ResponseEntity<String> responseEntity = userController.login("", "", response, "");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    
    }

    @Test
    public void loginWithInvalidUsernameReturns404Error() throws Exception {
        String invalidName = "invalid name";
        ResponseEntity<String> responseEntity = userController.login(invalidName, password, response, "");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
 
    }


    @Test
    public void loginWithInvalidPasswordReturns404Error() throws Exception {
        String invalidPassword = "invalid password";
        ResponseEntity<String> responseEntity = userController.login(name, invalidPassword, response, "");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void loginWithInvalidUsernameAndPasswordReturns404Error() throws Exception {
        String invalidName = "invalid name";
        String invalidPassword = "invalid password";
        ResponseEntity<String> responseEntity = userController.login(invalidName, invalidPassword, response, "");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    private ResponseEntity<String> doLogin(String inName, String inPassword, String inCookie) throws Exception {
        ResponseEntity<String> responseEntity = userController.login(inName, inPassword, response, inCookie);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(userRepository.findByNameAndPassword(name, password), is(notNullValue()));

        // Make sure login session has been added and associated with login
        assertThat(sessionRepository.findById(name), is(notNullValue()));

        User foundUser = userRepository.findByName(name); //Retrieve user
        assertThat(foundUser.getPassword(), is(password)); //Check password
        assertThat((double) foundUser.getBalance(), closeTo(balance, 0.001)); //Check balance

        // Make sure CSRF token exists
        assertThat(csrfRepository.findById(name), is(notNullValue()));

        return responseEntity; // maybe not necessary
    }

    @Test
    public void login() throws Exception {
        doLogin(name, password, "");
        
    }
    
    // test login with cookie
    @Test
    public void userWithCookie() throws Exception {        
        ResponseEntity<String> responseEntity =  doLogin(name, password, "");
        ResponseEntity<User> responseEntityUser;
        
        
        String csrf = responseEntity.getBody(); //CSRF
        String sessionToken = response.getCookies()[1].getValue(); //Session Token
        responseEntityUser = userController.getUser(sessionToken);
        assertThat(responseEntityUser.getStatusCode(), is(HttpStatus.OK));
    }

    // test where cookie is empty (i.e. "")
    @Test
    public void userWithEmptyCookie() throws Exception {
        ResponseEntity<User> responseEntityUser;
        
        responseEntityUser = userController.getUser("");
        assertThat(responseEntityUser.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }

    // test where cookie is invalid (i.e. random string)
    @Test
    public void userWithRandomCookie() throws Exception {
        ResponseEntity<User> responseEntityUser;
        String invalidCookie = "invalid cookie";
        
        responseEntityUser = userController.getUser(invalidCookie);
        assertThat(responseEntityUser.getStatusCode(), is(HttpStatus.FORBIDDEN));
    }
}

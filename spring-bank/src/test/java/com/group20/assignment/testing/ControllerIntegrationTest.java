package com.group20.assignment.testing;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;

import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.RandomTokenGenerator;
import com.group20.assignment.repository.UserRepository;
import com.group20.assignment.domain.User;
import com.group20.assignment.domain.CSRFToken;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerIntegrationTest {

    private MockMvc mvc;
    private UserRepository userRepository;
    private CSRFRepository csrfRepository;

    private final String TEST_API;

    private final String requesterName;
    private final String requesterPassword;
    private final float requesterStartBalance;

    private final String targetName;
    private final String targetPassword;
    private final float targetStartBalance;

    public ControllerIntegrationTest(MockMvc mvc, UserRepository userRepository, CSRFRepository csrfRepository,
            String TEST_API, String requesterName, String requesterPassword, float requesterStartBalance,
            String targetName, String targetPassword, float targetStartBalance) {

        this.mvc = mvc;
        this.userRepository = userRepository;
        this.csrfRepository = csrfRepository;

        this.TEST_API = TEST_API;

        this.requesterName = requesterName;
        this.requesterPassword = requesterPassword;
        this.requesterStartBalance = requesterStartBalance;

        this.targetName = targetName;
        this.targetPassword = targetPassword;
        this.targetStartBalance = targetStartBalance;
    }

    public MvcResult doLogin(String inName, String inPassword) throws Exception {
        return mvc
                .perform(get("/user/login?name={name}&password={password}", inName, inPassword)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    // Legitimate transaction
    public void validTransaction() throws Exception {
        float expectedRequesterBalance = 0;
        float expectedTargetBalance = requesterStartBalance;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        String csrf = requesterLoginResult.getResponse().getContentAsString();
        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, csrf).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isOk());

        // Make sure that the money was transferred
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction with invalid user
    public void transactionFailureTargetNotFound() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        String invalidUser = "invalidUser";

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, invalidUser,
                requesterStartBalance, csrf).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isNotFound());

        // Make sure invalidUser doesn't actually exist in DB
        assertThat(userRepository.findByName(invalidUser), nullValue());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
    }
    
    // Transaction where amount > requester balance
    public void transactionFailureNotEnoughFunds() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = targetStartBalance;
        float invalidAmount = requesterStartBalance * 2;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                invalidAmount, csrf).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isPaymentRequired());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction where amount is negative
    public void transactionFailureNegativeAmount() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = targetStartBalance;

        float invalidAmount = -requesterStartBalance;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                invalidAmount, csrf).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isUnprocessableEntity());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction where amount is zero
    public void transactionFailureZeroAmount() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = targetStartBalance;
        float invalidAmount = 0;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                invalidAmount, csrf).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isUnprocessableEntity());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction where no cookie is sent
    public void transactionFailureNoCookie() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = 0;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, csrf).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction where session_token cookie is empty (i.e. "")
    public void transactionFailureEmptyCookie() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = 0;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie fakeCookie = new Cookie("session_token", "");
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, csrf).contentType(MediaType.APPLICATION_JSON).cookie(fakeCookie))
                .andExpect(status().isUnauthorized());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Transaction where cookie is invalid (i.e. random string)
    public void transactionFailureInvalidCookie() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = 0;

        MvcResult requesterLoginResult = doLogin(requesterName, requesterPassword);

        Cookie fakeCookie = new Cookie("session_token", RandomTokenGenerator.requestToken());
        String csrf = requesterLoginResult.getResponse().getContentAsString();

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, csrf).contentType(MediaType.APPLICATION_JSON).cookie(fakeCookie))
                .andExpect(status().isUnauthorized());

        // Make sure no transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }
    
}
package com.group20.assignment.testing;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.NoSuchElementException;

import javax.servlet.http.Cookie;

import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.RandomTokenGenerator;
import com.group20.assignment.repository.UserRepository;
import com.group20.assignment.domain.User;
import com.group20.assignment.domain.CSRFToken;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(Lifecycle.PER_CLASS)
public class api2ControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CSRFRepository csrfRepository;

    private final String TEST_API = "api2";

    private final String requesterName = "victim";
    private final String requesterPassword = "password1";
    private final float requesterStartBalance = 50000f;

    private final String targetName = "attacker";
    private final String targetPassword = "password2";
    private final float targetStartBalance = 0f;

    private ControllerIntegrationTest controllerIntegrationTest;

    @BeforeAll
    private void init() {
        controllerIntegrationTest = new ControllerIntegrationTest(mvc, userRepository, csrfRepository, TEST_API,
                requesterName, requesterPassword, requesterStartBalance, targetName, targetPassword,
                targetStartBalance);
    }

    @BeforeEach
    private void resetDb() {
        userRepository.deleteAll();

        User user = new User(requesterName, requesterPassword, requesterStartBalance);
        userRepository.saveAndFlush(user);
        User user2 = new User(targetName, targetPassword, targetStartBalance);
        userRepository.saveAndFlush(user2);
    }

    // Legitimate transaction
    @Test
    public void validTransaction() throws Exception {
        controllerIntegrationTest.validTransaction();
    }

    // Transaction with invalid user
    @Test
    public void transactionFailureTargetNotFound() throws Exception {
        controllerIntegrationTest.transactionFailureTargetNotFound();
    }

    // Transaction where amount > requester balance
    @Test
    public void transactionFailureNotEnoughFunds() throws Exception {
        controllerIntegrationTest.transactionFailureNotEnoughFunds();
    }

    // Transaction where amount is negative
    @Test
    public void transactionFailureNegativeAmount() throws Exception {
        controllerIntegrationTest.transactionFailureNegativeAmount();
    }

    // Transaction where amount is zero
    @Test
    public void transactionFailureZeroAmount() throws Exception {
        controllerIntegrationTest.transactionFailureZeroAmount();
    }

    // Transaction where no cookie is sent
    @Test
    public void transactionFailureNoCookie() throws Exception {
        controllerIntegrationTest.transactionFailureNoCookie();
    }

    // Transaction where session_token cookie is empty (i.e. "")
    @Test
    public void transactionFailureEmptyCookie() throws Exception {
        controllerIntegrationTest.transactionFailureEmptyCookie();
    }

    // Transaction where cookie is invalid (i.e. random string)
    @Test
    public void transactionFailureInvalidCookie() throws Exception {
        controllerIntegrationTest.transactionFailureInvalidCookie();
    }

    // Baseline vulnerability: no CSRF
    @Test
    public void transaction2NotVulnerableTo1() throws Exception {
        float expectedRequesterBalance = requesterStartBalance;
        float expectedTargetBalance = targetStartBalance;

        MvcResult requesterLoginResult = controllerIntegrationTest.doLogin(requesterName, requesterPassword);

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}", TEST_API, targetName, requesterStartBalance)
                .contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isBadRequest());

        // Transaction Controller 2 makes sure that a CSRF token is send; make sure no
        // transfer took place
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Vulnerability 2: CSRF is a random token / String
    @Test
    public void transaction2VulnerableTo2() throws Exception {
        float expectedRequesterBalance = 0;
        float expectedTargetBalance = requesterStartBalance;

        MvcResult requesterLoginResult = controllerIntegrationTest.doLogin(requesterName, requesterPassword);

        String randomCSRF = RandomTokenGenerator.requestToken();

        // Make sure that the randomly generated CSRF doesn't exist
        assertThrows(NoSuchElementException.class, () -> csrfRepository.findById(randomCSRF).get());

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, randomCSRF).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isOk());

        // Transaction Controller 2 only checks that a CSRF token is sent; make sure
        // that the money was transferred
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Vulnerability 3: Empty CSRF (e.g. "")
    @Test
    public void transaction2VulnerableTo3() throws Exception {
        float expectedRequesterBalance = 0;
        float expectedTargetBalance = requesterStartBalance;

        MvcResult requesterLoginResult = controllerIntegrationTest.doLogin(requesterName, requesterPassword);

        String emptyCSRF = "";

        // Make sure that the randomly generated CSRF doesn't exist
        assertThrows(NoSuchElementException.class, () -> csrfRepository.findById(emptyCSRF).get());

        Cookie cookie = requesterLoginResult.getResponse().getCookie("session_token");

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, emptyCSRF).contentType(MediaType.APPLICATION_JSON).cookie(cookie))
                .andExpect(status().isOk());

        // Transaction Controller 2 only checks that a CSRF token is sent; make sure
        // that the money was transferred
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }

    // Vulnerability 4: Using CSRF from another user
    @Test
    public void transaction2VulnerableTo4() throws Exception {
        float expectedRequesterBalance = 0;
        float expectedTargetBalance = requesterStartBalance;

        MvcResult requesterLoginResult = controllerIntegrationTest.doLogin(requesterName, requesterPassword);
        Cookie requesterCookie = requesterLoginResult.getResponse().getCookie("session_token");

        MvcResult targetLoginResult = controllerIntegrationTest.doLogin(targetName, targetPassword);
        String targetCSRF = targetLoginResult.getResponse().getContentAsString();

        // Make sure that the CSRF that we got exists and doesn't belong to the
        // requester
        CSRFToken targetToken = csrfRepository.findById(targetCSRF).get();
        Long targetUserID = targetToken.getUserID();
        User targetUser = userRepository.findById(targetUserID).get();

        assertThat(targetUser.getName(), is(not(requesterName)));
        assertThat(targetUser.getPassword(), is(not(requesterPassword)));

        mvc.perform(get("/{api}/transfer?target={target}&amount={amount}&CSRF={CSRF}", TEST_API, targetName,
                requesterStartBalance, targetCSRF).contentType(MediaType.APPLICATION_JSON).cookie(requesterCookie))
                .andExpect(status().isOk());

        // Transaction Controller 2 only checks that a CSRF token is sent; make sure
        // that the money was transferred
        assertThat((double) userRepository.findByName(requesterName).getBalance(),
                closeTo(expectedRequesterBalance, 0.001));
        assertThat((double) userRepository.findByName(targetName).getBalance(), closeTo(expectedTargetBalance, 0.001));
    }
}

package com.group20.assignment.testing;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import javax.servlet.http.Cookie;

import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.RandomTokenGenerator;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;
import com.group20.assignment.domain.User;
import com.group20.assignment.domain.CSRFToken;
import com.group20.assignment.domain.Session;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class UserRestControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private CSRFRepository csrfRepository;

    private final String name = "test";
    private final String password = "password1";
    private final String name2 = "test2";
    private final String password2 = "password2";
    private final float balance = 500f;

    private Long id;

    @BeforeEach
    private void resetDb() {
        userRepository.deleteAll();

        User user = new User(name, password, balance);
        userRepository.saveAndFlush(user);

        id = userRepository.findByNameAndPassword(name, password).getID();
    }

    @Test
    public void loginWithNoDetailsReturns400Error() throws Exception {
        mvc.perform(get("/user/login").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void loginWithInvalidUsernameReturns404Error() throws Exception {
        mvc.perform(get("/user/login?name={name}&password={password}", "invalidName", password).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void loginWithInvalidPasswordReturns404Error() throws Exception {
        mvc.perform(get("/user/login?name={name}&password={password}", name, "invalidPassword").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void loginWithInvalidUsernameAndPasswordReturns404Error() throws Exception {
        mvc.perform(get("/user/login?name={name}&password={password}", "invalidName", "invalidPassword").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private MvcResult doLogin() throws Exception {
        return mvc.perform(get("/user/login?name={name}&password={password}", name, password).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
    }

    @Test
    public void login() throws Exception {
        MvcResult loginResult = doLogin();
        
        // Make sure that we actually got a token
        assertThat(loginResult.getResponse().getCookie("session_token"), notNullValue());

        // Make sure that we got a CSRF with the correct length
        String csrf = loginResult.getResponse().getContentAsString();
        assertThat(csrf.length(), is(15));

        // Make sure that the token we got sent back is legit
        String session_id = loginResult.getResponse().getCookie("session_token").getValue();
        Session foundSession = sessionRepository.getById(session_id);
        Long foundUserID = foundSession.getSessionUserID();
        User foundUser = userRepository.getById(foundUserID);

        assertThat(foundSession, notNullValue());

        assertThat(foundUser.getName(), is(name));
        assertThat(foundUser.getPassword(), is(password));
        assertThat((double) foundUser.getBalance(), closeTo(balance, 0.001));

        // Make sure that the CSRF that we got is legit
        CSRFToken token = csrfRepository.findById(csrf).get();
        Long tokenUserID = token.getUserID();
        User tokenUser = userRepository.findById(tokenUserID).get();

        assertThat(tokenUser.getName(), is(name));
        assertThat(tokenUser.getPassword(), is(password));
        assertThat((double) tokenUser.getBalance(), closeTo(balance, 0.001));
    }
    
    // test with victim's cookie
    @Test
    public void userWithCookie() throws Exception {        
        MvcResult loginResult = doLogin();
        Cookie cookie = loginResult.getResponse().getCookie("session_token");

        mvc.perform(get("/user/").contentType(MediaType.APPLICATION_JSON).cookie(cookie))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is(name)))
        .andExpect(jsonPath("$.password", is(password)))
        .andExpect(jsonPath("$.balance", closeTo(balance, 0.001)));
    }

    // test where cookie not provided
    @Test
    public void userWithoutCookie() throws Exception {
        mvc.perform(get("/user/").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isForbidden());
    }

    // test where cookie is empty (i.e. "")
    @Test
    public void userWithEmptyCookie() throws Exception {
        Cookie fakeCookie = new Cookie("session_token", "");
        fakeCookie.setMaxAge(60*60*24*7);

        mvc.perform(get("/user/").contentType(MediaType.APPLICATION_JSON).cookie(fakeCookie))
        .andExpect(status().isForbidden());
    }

    // test where cookie is invalid (i.e. random string)
    @Test
    public void userWithRandomCookie() throws Exception {
        Cookie fakeCookie = new Cookie("session_token", RandomTokenGenerator.requestToken());
        mvc.perform(get("/user/").contentType(MediaType.APPLICATION_JSON).cookie(fakeCookie))
        .andExpect(status().isForbidden());
    }

    /*@Test
    public void setUserBalance() throws Exception {
        int increaseAmount = 10000;

        mvc.perform(put("/user/balance/add?amount={amount}", id, increaseAmount).contentType(MediaType.APPLICATION_JSON))
                // .andDo(print())
                .andExpect(status().isOk());
        
        var foundUser = userRepository.findById(id).orElseThrow(RuntimeException::new);

        assertThat((double) foundUser.getBalance(), closeTo(balance + increaseAmount, 0.001));
    }*/

    /*@Test
    public void shouldThrowExceptionBecauseNameIsIncorrect() {
        var exception = assertThrows(AssertionError.class,
                () -> mvc.perform(get("/user/{id}", id).contentType(MediaType.APPLICATION_JSON))
                        // .andDo(print())
                        .andExpect(status().isOk())
                        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("$.name", is("not" + name))));
        
        assertThat(exception.getMessage(), containsString("JSON path \"$.name\""));
    }*/
}

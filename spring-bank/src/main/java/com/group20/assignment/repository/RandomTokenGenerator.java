package com.group20.assignment.repository;

import java.security.SecureRandom;

public class RandomTokenGenerator {

    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int TOKEN_LENGTH = 15;

    private static SecureRandom sr = new SecureRandom();

    /**
     * Generates a random string of upper and lower case alphanumeric characters.
     * Doesn't check for collisions or anything; you'll have to do that yourself
     * 
     * @param length
     *        the length of the String to generate
     * @param random
     *        the random generator to use (SecureRandom highly recommended)
     * @return the generated String
     */
    public static String requestToken(int length) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            sb.append(CHARACTERS.charAt(sr.nextInt(CHARACTERS.length())));
        }

        return sb.toString();
    }

    public static String requestToken() {
        return requestToken(TOKEN_LENGTH);
    }
}

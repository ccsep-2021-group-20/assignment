package com.group20.assignment.repository;

import com.group20.assignment.domain.Session;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, String> {
    Session findByIdAndUserID(String id, Long userID);
}

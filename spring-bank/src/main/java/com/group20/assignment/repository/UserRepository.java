package com.group20.assignment.repository;

import com.group20.assignment.domain.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByNameAndPassword(String name, String password);
    User findByName(String name);
}

package com.group20.assignment.repository;

import com.group20.assignment.domain.CSRFToken;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CSRFRepository extends JpaRepository<CSRFToken, String> {
    CSRFToken findByIdAndUserID(String id, String userID);
}

package com.group20.assignment;

import com.group20.assignment.domain.User;
import com.group20.assignment.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapInitialData implements CommandLineRunner {
    
    private final UserRepository userRepository;

    @Autowired
    public BootstrapInitialData(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        userRepository.save(new User("Attacker", "1337h4x0r", 0f));
        userRepository.save(new User("Victim", "password1", 50000f));
    }
}

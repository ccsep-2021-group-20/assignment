package com.group20.assignment.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.group20.assignment.repository.RandomTokenGenerator;

@Entity
@Table(name = "csrf_tokens")
public class CSRFToken {
    @Id
    private String id;

    private Long userID;

    CSRFToken() {}

    public CSRFToken(Long userID) {
        this.id = RandomTokenGenerator.requestToken();
        this.userID = userID;
    }

    public Long getUserID() {
        return userID;
    }

    public String getToken() {
        return id;
    }
}

package com.group20.assignment.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.group20.assignment.repository.RandomTokenGenerator;

@Entity
@Table(name = "sessions")
public class Session {

    @Id
    private String id;

    private Long userID;
    private Long sessionTime;

    Session() {}

    public Session(Long sessionUserID) {
        this.id = RandomTokenGenerator.requestToken();
        this.userID = sessionUserID;
        this.sessionTime = System.currentTimeMillis();
    }

    public String getID() {
        return id;
    }

    public Long getSessionTime() {
        return sessionTime;
    }

    // TODO: either remove this method or use it every time a user makes a valid
    // request (i.e. reset the time the session was last created / used)
    public void updateSessionTime() {
        this.sessionTime = System.currentTimeMillis();
    }

    public Long getSessionUserID() {
        return userID;
    }
}

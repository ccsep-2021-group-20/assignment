package com.group20.assignment.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String password;
    private float balance;

    User() {}

    public User(String name, String password, float balance) {
        this.name = name;
        this.password = password;
        this.balance = balance;
    }

    public Long getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public float getBalance() {
        return balance;
    }

    /**
     * Adds the amount specified to this user's account. Can be negative to remove
     * money
     * 
     * @param amount
     *        the amount to add (negative to remove)
     */
    public void updateBalance(float amount) {
        balance += amount;
    }
}

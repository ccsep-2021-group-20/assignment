package com.group20.assignment.controller;

import com.group20.assignment.domain.User;
import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

// Controller that checks if the CSRF token exists, but only if the length of
// the token is greater than 0.
@RestController
@RequestMapping("/api3")
public class api3Controller {

    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;
    private final CSRFRepository csrfRepository;

    public api3Controller(UserRepository userRepository, SessionRepository sessionRepository,
            CSRFRepository csrfRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
        this.csrfRepository = csrfRepository;
    }

    @GetMapping("/transfer")
    public ResponseEntity<Float> getMethodName(@RequestParam String target, @RequestParam float amount,
            @RequestParam String CSRF,
            @CookieValue(value = "session_token", defaultValue = "null") String sessionCookie) {

        // Make sure sent session token exists in DB
        if (!sessionRepository.findById(sessionCookie).isPresent()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        // Make sure a CSRF token was sent and it exists in the CSRF DB
        // VULNERABILITY HERE
        if (0 < CSRF.length() && csrfRepository.findById(CSRF).isPresent() == false) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Make sure the target user exists in the DB
        User targetUser = userRepository.findByName(target);
        if (targetUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Make sure that the amount to send is positive (can't transfer negative
        // funds!)
        if (amount <= 0) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        User requestUser = userRepository.findById(sessionRepository.findById(sessionCookie).get().getSessionUserID())
                .get();

        if (requestUser.getBalance() < amount) {
            return new ResponseEntity<>(HttpStatus.PAYMENT_REQUIRED);
        }

        requestUser.updateBalance(-amount);
        targetUser.updateBalance(amount);

        userRepository.saveAndFlush(requestUser);
        userRepository.saveAndFlush(targetUser);

        return ResponseEntity.ok(requestUser.getBalance());
    }
}

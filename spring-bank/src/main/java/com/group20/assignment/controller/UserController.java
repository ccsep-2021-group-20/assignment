package com.group20.assignment.controller;

import com.group20.assignment.domain.CSRFToken;
import com.group20.assignment.domain.Session;
import com.group20.assignment.domain.User;
import com.group20.assignment.repository.CSRFRepository;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class UserController {

    private final int sessionExpiryTime = 60 * 60 * 24 * 7;

    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;
    private final CSRFRepository csrfRepository;

    public UserController(UserRepository userRepository, SessionRepository sessionRepository,
            CSRFRepository csrfRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
        this.csrfRepository = csrfRepository;
    }

    // Get user
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(
            @CookieValue(value = "session_token", defaultValue = "null") String sessionCookie) {

        if (!sessionRepository.findById(sessionCookie).isPresent()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Long userID = sessionRepository.findById(sessionCookie).get().getSessionUserID();

        return ResponseEntity.ok(userRepository.findById(userID).get());
    }

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@RequestParam String name, @RequestParam String password,
            HttpServletResponse response,
            @CookieValue(value = "session_token", defaultValue = "null") String existingToken) {

        // TODO: do we still need this?
        // If a user tries to log in but they already have a cookie
        if (!existingToken.equals("null")) {
            Cookie removeCookie = new Cookie("session_token", "");
            removeCookie.setMaxAge(0);
            response.addCookie(removeCookie);
        }

        User user = userRepository.findByNameAndPassword(name, password);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Create a session for the user
        Session newSession = new Session(user.getID());
        sessionRepository.saveAndFlush(newSession);

        // Create a new cookie to send to the user
        Cookie sessionCookie = new Cookie("session_token", newSession.getID());

        sessionCookie.setMaxAge(sessionExpiryTime);
        sessionCookie.setPath("/");
        response.addCookie(sessionCookie);

        // Generate a CSRF token
        CSRFToken csrfToken = new CSRFToken(user.getID());
        csrfRepository.saveAndFlush(csrfToken);

        return ResponseEntity.ok(csrfToken.getToken());
    }
}

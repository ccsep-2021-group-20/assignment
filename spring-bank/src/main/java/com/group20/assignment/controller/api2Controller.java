package com.group20.assignment.controller;

import com.group20.assignment.domain.User;
import com.group20.assignment.repository.SessionRepository;
import com.group20.assignment.repository.UserRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

// Controller with no CSRF checks; only cares that a token is passed
@RestController
@RequestMapping("/api2")
public class api2Controller {

    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;

    public api2Controller(UserRepository userRepository, SessionRepository sessionRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }

    @GetMapping("/transfer")
    public ResponseEntity<Float> getMethodName(@RequestParam String target, @RequestParam float amount,
            @RequestParam String CSRF,
            @CookieValue(value = "session_token", defaultValue = "null") String sessionCookie) {

        // Make sure sent session token exists in DB
        if (!sessionRepository.findById(sessionCookie).isPresent()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        // Make sure that a CSRF token was sent
        if (CSRF == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        // Make sure the target user exists in the DB
        User targetUser = userRepository.findByName(target);
        if (targetUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Make sure that the amount to send is positive (can't transfer negative
        // funds!)
        if (amount <= 0) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        User requestUser = userRepository.findById(sessionRepository.findById(sessionCookie).get().getSessionUserID())
                .get();

        // Make sure the user has enough money in their account to perform the
        // transaction
        if (requestUser.getBalance() < amount) {
            return new ResponseEntity<>(HttpStatus.PAYMENT_REQUIRED);
        }

        requestUser.updateBalance(-amount);
        targetUser.updateBalance(amount);

        userRepository.saveAndFlush(requestUser);
        userRepository.saveAndFlush(targetUser);

        return ResponseEntity.ok(requestUser.getBalance());
    }

}
